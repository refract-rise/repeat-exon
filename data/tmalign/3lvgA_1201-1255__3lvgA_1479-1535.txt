
 **************************************************************************
 *                        TM-align (Version 20170708)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/3lvgA_1201-1255.pdb                      
Name of Chain_2: data/pdb/3lvgA_1479-1535.pdb                      
Length of Chain_1:   55 residues
Length of Chain_2:   57 residues

Aligned length=   41, RMSD=   3.22, Seq_ID=n_identical/n_aligned= 0.049
TM-score= 0.37838 (if normalized by length of Chain_1)
TM-score= 0.37296 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
VGDRCYD-E---K-M-YD-AAKLLYNNV--SN--FGR-LAS-TLVHLGEYQA-AVDGAR-K-ANSTRTWKE
  .: :: :   . . :: :: ::::::  ::  : : ::. .:::: :: : :: : : : ::::     
--AL-RTSIDAYDNFDNISLA-QRLEKHELIEFRR-IAAYLFKGNNR-WK-QSVE-L-CKKDSLYK-----

