
 **************************************************************************
 *                        TM-align (Version 20150914)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/4u4jA_284-377.pdb                        
Name of Chain_2: data/pdb/4u4jA_750-846.pdb                        
Length of Chain_1:   94 residues
Length of Chain_2:   97 residues

Aligned length=   73, RMSD=   3.12, Seq_ID=n_identical/n_aligned= 0.137
TM-score= 0.55958 (if normalized by length of Chain_1)
TM-score= 0.54579 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
IGVSAHR--YH--NRLDIFTQSLENGAAQSGIEIVLLNDKGQTLAQA-TSDA--QGHVQLEA-D-KA-A-ALLLARKE------------EQTTLLDLTLPALDLSEFNVAGAPGYS-
:::::::  ::  ::::::::: :.  ...:::::::  :::::::: :...  .::::::: : :: . ::::::::            :::::::::..                 
VTLKLDKANYRPGDTMKLHIAA-PV--AGKGYAMVES--SDGPLWWQAIDVPAQGLELTIPVDKTWNRHDLYLSTLVVRPGDKSRSATPKRAVGLLHLPLG----------------D

