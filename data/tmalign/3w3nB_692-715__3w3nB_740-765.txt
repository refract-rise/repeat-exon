
 **************************************************************************
 *                        TM-align (Version 20170708)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/3w3nB_692-715.pdb                        
Name of Chain_2: data/pdb/3w3nB_740-765.pdb                        
Length of Chain_1:   24 residues
Length of Chain_2:   26 residues

Aligned length=   24, RMSD=   2.49, Seq_ID=n_identical/n_aligned= 0.375
TM-score= 0.43358 (if normalized by length of Chain_1)
TM-score= 0.46046 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
LLDLRGNKLLFLTDS-LSDF-TSSLR
::::::::::::::. :.:. .::::
HLDLSSNLLKTINKSALETKTTTKLS

