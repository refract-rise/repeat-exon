
 **************************************************************************
 *                        TM-align (Version 20170708)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/3w3gA_45-65.pdb                          
Name of Chain_2: data/pdb/3w3gA_91-127.pdb                         
Length of Chain_1:   21 residues
Length of Chain_2:   26 residues

Aligned length=   19, RMSD=   2.50, Seq_ID=n_identical/n_aligned= 0.000
TM-score= 0.21618 (if normalized by length of Chain_1)
TM-score= 0.28068 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
VIAECSNRR---LQEVP--QTV-GK-YV
 ::::::::   .:::.  : : .: ::
-KINLNHNPNVNGLNITDGA-FLNLKNL

