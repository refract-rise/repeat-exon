
 **************************************************************************
 *                        TM-align (Version 20170708)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/4g8aB_58-80.pdb                          
Name of Chain_2: data/pdb/4g8aB_573-603.pdb                        
Length of Chain_1:   23 residues
Length of Chain_2:   31 residues

Aligned length=   18, RMSD=   2.28, Seq_ID=n_identical/n_aligned= 0.278
TM-score= 0.30810 (if normalized by length of Chain_1)
TM-score= 0.30946 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
NLDLSFNPLRH---LGSYS--FFS---FPE--L---
:::::::::::   ::: :  :     .    :   
FLNLTQNDFACTCEHQS-FLQW--IKDQ--RQLLVE

