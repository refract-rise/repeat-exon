
 **************************************************************************
 *                        TM-align (Version 20150914)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/4ui9A_72-125.pdb                         
Name of Chain_2: data/pdb/4ui9A_1688-1737.pdb                      
Length of Chain_1:   54 residues
Length of Chain_2:   32 residues

Aligned length=   18, RMSD=   2.93, Seq_ID=n_identical/n_aligned= 0.000
TM-score= 0.21084 (if normalized by length of Chain_1)
TM-score= 0.26195 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
ESWQLRKGVSEIGE-DVD-YDEELYVAGNMVIWSKGSKSQALAVYKAFTVDSPVQQ------------
         :  .  .:: .::::::::                        ::::            
---------K--S-ILSKDGVLYVKLRA------------------------GQLSYKEDNSEARAFK

