
 **************************************************************************
 *                        TM-align (Version 20170708)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/4hb4C_10-44.pdb                          
Name of Chain_2: data/pdb/4hb4C_182-225.pdb                        
Length of Chain_1:   35 residues
Length of Chain_2:   41 residues

Aligned length=   33, RMSD=   1.81, Seq_ID=n_identical/n_aligned= 0.152
TM-score= 0.57353 (if normalized by length of Chain_1)
TM-score= 0.53346 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
---D--LDIALLDQVVSTFYQG-SGVQQKQAQEILTKFQDN--
   :  ::: :::: ::::::: ::::::::::::::::::  
LKNSMKEFE-IFKL-FQVLEQGSSSSLIVATLESLLRYLHWIP

