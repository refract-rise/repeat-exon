
 **************************************************************************
 *                        TM-align (Version 20170708)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/4xeiC_131-167.pdb                        
Name of Chain_2: data/pdb/4xeiC_361-372.pdb                        
Length of Chain_1:   37 residues
Length of Chain_2:   12 residues

Aligned length=    9, RMSD=   1.36, Seq_ID=n_identical/n_aligned= 0.111
TM-score= 0.16521 (if normalized by length of Chain_1)
TM-score= 0.25728 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
WWVCKHIKKPIRSTVLSLDW--H-PNSVLLAAGSCDFKCR
                      : ::::::::        
--------------------SLESALKDLKIV--------

