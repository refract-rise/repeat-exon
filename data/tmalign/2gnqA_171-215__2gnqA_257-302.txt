
 **************************************************************************
 *                        TM-align (Version 20170708)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/2gnqA_171-215.pdb                        
Name of Chain_2: data/pdb/2gnqA_257-302.pdb                        
Length of Chain_1:   43 residues
Length of Chain_2:   45 residues

Aligned length=   40, RMSD=   2.01, Seq_ID=n_identical/n_aligned= 0.250
TM-score= 0.68665 (if normalized by length of Chain_1)
TM-score= 0.66415 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
SDP--VSAVHFNRDG-SLIVSSSYDGL-RIWDTASG-CLKTLIDDDNP
 ::  .:::::::.  ::::::::::: :::::::: ::::::: :::
-NEKYCIFANFSVT-GGWIVSGSEDNLVYIWNLQTKEIVQKLQG-HTD

