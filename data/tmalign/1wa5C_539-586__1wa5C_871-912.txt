
 **************************************************************************
 *                        TM-align (Version 20170708)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/1wa5C_539-586.pdb                        
Name of Chain_2: data/pdb/1wa5C_871-912.pdb                        
Length of Chain_1:   48 residues
Length of Chain_2:   37 residues

Aligned length=   22, RMSD=   3.53, Seq_ID=n_identical/n_aligned= 0.045
TM-score= 0.23984 (if normalized by length of Chain_1)
TM-score= 0.25837 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
KEDISNSTEILLKNLIALILKH---------GSSPEK-LAENEFLMRSIFRVLQTSED-----
:: :::::  :  :  .              ::::.  .: :::. .                
SI-ANLKN--D--Y--V-----EEISTFGSHFSKLV-SIS-EKPF-D-----------PLPEI

