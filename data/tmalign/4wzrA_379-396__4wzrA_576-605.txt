
 **************************************************************************
 *                        TM-align (Version 20150914)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/4wzrA_379-396.pdb                        
Name of Chain_2: data/pdb/4wzrA_576-605.pdb                        
Length of Chain_1:   18 residues
Length of Chain_2:   30 residues

Aligned length=   14, RMSD=   1.96, Seq_ID=n_identical/n_aligned= 0.143
TM-score= 0.42227 (if normalized by length of Chain_1)
TM-score= 0.31469 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
-DRKVIVKTMKTYVEKV--AN-------------
 :::::::::: .:  :  :              
CFAKTLVEHVG-MK--NLKS-WASVNRGAIILSS

