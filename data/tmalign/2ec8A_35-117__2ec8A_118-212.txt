
 **************************************************************************
 *                        TM-align (Version 20170708)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/2ec8A_35-117.pdb                         
Name of Chain_2: data/pdb/2ec8A_118-212.pdb                        
Length of Chain_1:   83 residues
Length of Chain_2:   91 residues

Aligned length=   74, RMSD=   3.58, Seq_ID=n_identical/n_aligned= 0.108
TM-score= 0.53983 (if normalized by length of Chain_1)
TM-score= 0.50818 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
SPPSIHPGKSDLIVRVGDEIRLLC-TDP-GF-V-KWTFEIL-DETNE----NKQN---EWITEKAEATNT-GKYTCTNKHGLSNSIYVFVRDPAKL----
  :::  ::: ::::::::::::: ::: .. . :::::   ..::.    ..::   :::::::::::: ::::::....:::::::::::..      
--FLV--DRS-LYGKEDNDTLVRCPLTDPEVTNYSLKGC--QGKPLPKDLRFIPDPKAGIMIKSVKRAYHRLCLHCSVDSVLSEKFILKVRPAF--KAVP

