
 **************************************************************************
 *                        TM-align (Version 20170708)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/3s51B_225-295.pdb                        
Name of Chain_2: data/pdb/3s51B_598-662.pdb                        
Length of Chain_1:   64 residues
Length of Chain_2:   65 residues

Aligned length=   47, RMSD=   3.10, Seq_ID=n_identical/n_aligned= 0.106
TM-score= 0.46221 (if normalized by length of Chain_1)
TM-score= 0.45769 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
SRRSVLDGIIAFFRELDKQHREEQSSTAPADELYHVEGTVILHIVFA---------IKLDCELGR--------ELLK-HLKA
    : ::::::::::::::::::   :::::::::::::::: :.          .:.:..:          ..   .   
----A-DIRLMLYDGFYDVLRRNS---QLASSIMQTLFSQLKQ-FY-EPEPDLLPPLKLGACV--LTQGSQIFLQ--EP---

