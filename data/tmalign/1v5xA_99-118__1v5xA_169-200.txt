
 **************************************************************************
 *                        TM-align (Version 20150914)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/1v5xA_99-118.pdb                         
Name of Chain_2: data/pdb/1v5xA_169-200.pdb                        
Length of Chain_1:   20 residues
Length of Chain_2:   32 residues

Aligned length=   19, RMSD=   2.36, Seq_ID=n_identical/n_aligned= 0.053
TM-score= 0.22084 (if normalized by length of Chain_1)
TM-score= 0.27049 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
VIKAFP---L-E--G-PA-R-PEWA-DY--PA-
::::::   : :  : :: : :::: ::  .  
YALDLASGVEEAPGVKSAEKLRALFARLASL-R

