
 **************************************************************************
 *                        TM-align (Version 20170708)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/1h2xA_199-239.pdb                        
Name of Chain_2: data/pdb/1h2xA_339-405.pdb                        
Length of Chain_1:   41 residues
Length of Chain_2:   67 residues

Aligned length=   30, RMSD=   2.12, Seq_ID=n_identical/n_aligned= 0.167
TM-score= 0.40774 (if normalized by length of Chain_1)
TM-score= 0.31680 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
GTETS-----------------TNLHQKLYYHVLGTDQSEDILCAEFPDEPKWMG-GAE-------------------
                      ::: :::::::::::: ::::::::   : ::: :::                   
-----EWVACVRSNFLVLCYLHDVK-NTLQLHDLATGA-LLKIFPLE---V-GSVVGYSGQKKDTEIFYQFTSFLSPG

