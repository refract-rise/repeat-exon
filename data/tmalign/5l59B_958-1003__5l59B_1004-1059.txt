
 **************************************************************************
 *                        TM-align (Version 20150914)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/5l59B_958-1003.pdb                       
Name of Chain_2: data/pdb/5l59B_1004-1059.pdb                      
Length of Chain_1:   46 residues
Length of Chain_2:   56 residues

Aligned length=   32, RMSD=   3.09, Seq_ID=n_identical/n_aligned= 0.062
TM-score= 0.32906 (if normalized by length of Chain_1)
TM-score= 0.30432 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
TPTFYRVSPSRGPLSGGTWIGIEGSHLNAGSDVAVSIG----GRPC--SFSW------------------
          :::  : :.:::::: :.::::::::::    ::::  ::.:                  
----------RNS--R-EIRCLTPP-GHTPGSAPIVININRAQLSNPEVKYNYTEDPTILRIDPEWSINS

