
 **************************************************************************
 *                        TM-align (Version 20150914)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/1hv9A_134-215.pdb                        
Name of Chain_2: data/pdb/1hv9A_308-324.pdb                        
Length of Chain_1:   82 residues
Length of Chain_2:   17 residues

Aligned length=   12, RMSD=   2.12, Seq_ID=n_identical/n_aligned= 0.083
TM-score= 0.11133 (if normalized by length of Chain_1)
TM-score= 0.18993 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
DDPTGYGRITRENGKVTGIVEHKDATDEQRQIQEINTGILIANGADMKRWLAKLTNNNAQGEYYI--TDIIALAYQEGREIVAV---
                       ::::::  :.:.                                ::                  
-----------------------EISPYT--VVED------------------------------ANLA---------------AAC

