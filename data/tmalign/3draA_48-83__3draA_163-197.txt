
 **************************************************************************
 *                        TM-align (Version 20150914)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/3draA_48-83.pdb                          
Name of Chain_2: data/pdb/3draA_163-197.pdb                        
Length of Chain_1:   36 residues
Length of Chain_2:   35 residues

Aligned length=   33, RMSD=   1.72, Seq_ID=n_identical/n_aligned= 0.182
TM-score= 0.70388 (if normalized by length of Chain_1)
TM-score= 0.71638 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
YSERALHITELGINELASHYTIWIYRFNILK-NL-PNR
  ::::::::::::::::::::::::::::: :. :: 
--AKELSFVDKVIDTDLKNNSAWSHRFFLLFSKKHLA-

