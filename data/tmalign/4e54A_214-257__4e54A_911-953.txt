
 **************************************************************************
 *                        TM-align (Version 20170708)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/4e54A_214-257.pdb                        
Name of Chain_2: data/pdb/4e54A_911-953.pdb                        
Length of Chain_1:   44 residues
Length of Chain_2:   43 residues

Aligned length=   36, RMSD=   2.35, Seq_ID=n_identical/n_aligned= 0.111
TM-score= 0.47635 (if normalized by length of Chain_1)
TM-score= 0.48111 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
AEASMVIAVPEPFGGAIIIG-QES-ITYHN----GDKYL-AIAPPIIKQST
  ::::::::   ::::::: ::: :::::    ::::: .:.: : : ::
--ALYLKTKG---DFILVGDLMRSVLLLAYKPMEGNFEEIARDF-N-P-NW

