
 **************************************************************************
 *                        TM-align (Version 20170708)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/4a0cA_977-1009.pdb                       
Name of Chain_2: data/pdb/4a0cA_1121-1156.pdb                      
Length of Chain_1:   33 residues
Length of Chain_2:   36 residues

Aligned length=   23, RMSD=   2.07, Seq_ID=n_identical/n_aligned= 0.043
TM-score= 0.43019 (if normalized by length of Chain_1)
TM-score= 0.41861 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
GSSYARSSVVTAVKFTISDH-PQPI-DPLLKN-CIG----------
      ::::::::::::.  :: : ::: :: : :          
------MLTFLMLVRLSTL-CPS-AVLQR-LDRL-VEPLRATCTTK

