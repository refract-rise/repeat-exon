
 **************************************************************************
 *                        TM-align (Version 20170708)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/2hyeA_375-409.pdb                        
Name of Chain_2: data/pdb/2hyeA_1038-1072.pdb                      
Length of Chain_1:   35 residues
Length of Chain_2:   35 residues

Aligned length=   15, RMSD=   1.84, Seq_ID=n_identical/n_aligned= 0.133
TM-score= 0.22634 (if normalized by length of Chain_1)
TM-score= 0.22634 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
LVTCSGAF------KEGS--LR--I--IR--NGIGIHEHASIDLPGIKG------
              ::::  ::  :  ::  ::::::                  
--------GLVTSLSESWYNLLLDMQNRLNKVIKSVG------------KIEHSF

