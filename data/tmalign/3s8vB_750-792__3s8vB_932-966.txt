
 **************************************************************************
 *                        TM-align (Version 20170708)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/3s8vB_750-792.pdb                        
Name of Chain_2: data/pdb/3s8vB_932-966.pdb                        
Length of Chain_1:   43 residues
Length of Chain_2:   35 residues

Aligned length=   26, RMSD=   2.42, Seq_ID=n_identical/n_aligned= 0.077
TM-score= 0.34968 (if normalized by length of Chain_1)
TM-score= 0.37206 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
PRALALDPAE--G-FMYWTEWGGKPKIDRAAMDGSERTTLVPNVGR------
..:::::: :  : :::::::   :::::::::                   
PTTFLLFS-QKSAINRMVIDE---QQSPDIILP-------------IHSLRN

