
 **************************************************************************
 *                        TM-align (Version 20150914)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/2iejA_68-96.pdb                          
Name of Chain_2: data/pdb/2iejA_283-339.pdb                        
Length of Chain_1:   29 residues
Length of Chain_2:   57 residues

Aligned length=   19, RMSD=   2.33, Seq_ID=n_identical/n_aligned= 0.158
TM-score= 0.23510 (if normalized by length of Chain_1)
TM-score= 0.21047 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
DRAEWADID----------PV--P-Q-NDGPN-PVV--QI--IY--SDKF-----------------
                   ::  : : ::::: :::  ::  ::  :::                  
---------ILQDRGLSKYPNLLNQLLDLQPSHSSPYLIAFLVDIYEDM-LENQCDNKEDILNKALE

