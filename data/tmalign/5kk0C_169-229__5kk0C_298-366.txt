
 **************************************************************************
 *                        TM-align (Version 20150914)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/5kk0C_169-229.pdb                        
Name of Chain_2: data/pdb/5kk0C_298-366.pdb                        
Length of Chain_1:   61 residues
Length of Chain_2:   69 residues

Aligned length=   46, RMSD=   2.87, Seq_ID=n_identical/n_aligned= 0.043
TM-score= 0.45951 (if normalized by length of Chain_1)
TM-score= 0.42597 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
DLGGILAEGQPLSAHPRIDPASTFDGG--QPCYVTFSI--K-----------SSLS-S-TLTLLELDP-----QGKLLRQ-KTE
 :   :    :::::::::::     :  :::::::::  .           .::: . :::::::::     :::: :: :: 
-Q---A----GFVFHHANAFE-----ENGKIILDSICYNSLPQVDTDGDFRSTNFDNLDPGQLWRFTIDPAAATVEK-QLMVS-

