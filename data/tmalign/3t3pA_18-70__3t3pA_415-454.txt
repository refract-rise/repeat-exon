
 **************************************************************************
 *                        TM-align (Version 20170708)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/3t3pA_18-70.pdb                          
Name of Chain_2: data/pdb/3t3pA_415-454.pdb                        
Length of Chain_1:   52 residues
Length of Chain_2:   40 residues

Aligned length=   34, RMSD=   2.14, Seq_ID=n_identical/n_aligned= 0.294
TM-score= 0.50871 (if normalized by length of Chain_1)
TM-score= 0.61977 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
-QFGFSLDF-HKDSH-G-VAIVVGAPRTLGPSQEETGGVFLCPWR-AEGGQCPSLLF-
 :::::::: :::.: : ::::::::::        ::::::::. .           
SAFGFSLRGAVDIDDNGYPDLIVGAYGA--------NQVAVYRAQPV----------V

