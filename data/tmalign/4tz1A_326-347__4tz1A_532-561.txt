
 **************************************************************************
 *                        TM-align (Version 20150914)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/4tz1A_326-347.pdb                        
Name of Chain_2: data/pdb/4tz1A_532-561.pdb                        
Length of Chain_1:   22 residues
Length of Chain_2:   30 residues

Aligned length=   20, RMSD=   2.93, Seq_ID=n_identical/n_aligned= 0.050
TM-score= 0.15104 (if normalized by length of Chain_1)
TM-score= 0.24453 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
GLHLLFTPGVYHV---------DQP-IEIDRA
::::::.  .:::         :.: ::::.:
THEGWGM--GSYCYFNVNPDIRQQHGFQAPVK

