
 **************************************************************************
 *                        TM-align (Version 20170708)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/3fcuA_302-373.pdb                        
Name of Chain_2: data/pdb/3fcuA_434-449.pdb                        
Length of Chain_1:   72 residues
Length of Chain_2:   16 residues

Aligned length=   16, RMSD=   0.37, Seq_ID=n_identical/n_aligned= 0.375
TM-score= 0.21884 (if normalized by length of Chain_1)
TM-score= 0.71871 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
GRHDLLVGAPLYMESRADRKLAEVGRVYLFLQPRGPHALGAPSLLLTGTQLYGRFGSAIAPLGDLDRDGYND
   :::::::::            :::::::                                         
---DLIVGAYGA------------NQVAVYR-----------------------------------------

