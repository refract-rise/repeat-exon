
 **************************************************************************
 *                        TM-align (Version 20170708)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/3e0gA_286-385.pdb                        
Name of Chain_2: data/pdb/3e0gA_386-480.pdb                        
Length of Chain_1:  100 residues
Length of Chain_2:   95 residues

Aligned length=   85, RMSD=   2.51, Seq_ID=n_identical/n_aligned= 0.106
TM-score= 0.64260 (if normalized by length of Chain_1)
TM-score= 0.66866 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
QQLNCETHDLKEIICSWNPGRVTAL-VGPRATSYTLVESF---SGKYVRLKRAEAPTN-ESYQLLFQMLPNQE-IYNFTLNAHN---PLGRSQS-TILVNITEKVYPHTP
:::::::::::::::::::: :..  .:  .:::::::::   .::::::::: : :: ::::::::::: :: :::::::::.   ::::::: :::::::        
TSFKVKDINSTAVKLSWHLP-GNF-AKI--NFLCEIEIKKSNSVQEQRNVTIQ-G-VENSSYLVALDKLN-PYTLYTFRIRCSTETFWKWSKWSNKKQHLTT--------

