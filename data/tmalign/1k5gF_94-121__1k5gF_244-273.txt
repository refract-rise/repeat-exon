
 **************************************************************************
 *                        TM-align (Version 20150914)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/1k5gF_94-121.pdb                         
Name of Chain_2: data/pdb/1k5gF_244-273.pdb                        
Length of Chain_1:   28 residues
Length of Chain_2:   30 residues

Aligned length=   26, RMSD=   1.93, Seq_ID=n_identical/n_aligned= 0.192
TM-score= 0.37942 (if normalized by length of Chain_1)
TM-score= 0.39233 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
PKLHTVRLSDNAFGPTA-QEPLI-DFLS-KH-
::::::::::::::::: :: :: : :: :. 
PNLRELGLNDCLLSARGAAA-VVDA-FSKLEN

