
 **************************************************************************
 *                        TM-align (Version 20170708)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/3mzkD_9-54.pdb                           
Name of Chain_2: data/pdb/3mzkD_206-255.pdb                        
Length of Chain_1:   46 residues
Length of Chain_2:   50 residues

Aligned length=   43, RMSD=   1.79, Seq_ID=n_identical/n_aligned= 0.209
TM-score= 0.72542 (if normalized by length of Chain_1)
TM-score= 0.68019 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
NELIHDAVLDYY---GKRLATCSSDKTIKIFEV-EGE-THKLIDTL--TGHEG
  ::::::::::   .::::::::::::::::: ::: :::: :::  :::::
--WVRDVAWSPTVLLRSYLASVSQDRTCIIWTQDNEQGPWKK-TLLKEEKFPD

