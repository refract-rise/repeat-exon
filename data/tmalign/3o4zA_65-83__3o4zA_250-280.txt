
 **************************************************************************
 *                        TM-align (Version 20170708)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: data/pdb/3o4zA_65-83.pdb                          
Name of Chain_2: data/pdb/3o4zA_250-280.pdb                        
Length of Chain_1:   15 residues
Length of Chain_2:   31 residues

Aligned length=   14, RMSD=   1.75, Seq_ID=n_identical/n_aligned= 0.071
TM-score= 0.62407 (if normalized by length of Chain_1)
TM-score= 0.37412 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
----------FTFLCQIVTFSRTIG-------
          :::::::::::::.        
ISLSVLFEIQSLPLKEVIVRLMSN-HSSTKFV

