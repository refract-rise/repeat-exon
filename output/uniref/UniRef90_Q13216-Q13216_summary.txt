Total UniProts in cluster 33
Total Eukaryotic 33
Total with exons 22
Organisms Pongo,Mandrillus,Nomascus,Rhinopithecus,Pan,Callithrix,Macaca,Papio,Saimiri,Chlorocebus,Homo,Colobus,Gorilla,Cercocebus,Aotus
Organism details A0A096MRY4-Papio,A0A0D9RU35-Chlorocebus,A0A2K5C8K9-Aotus,A0A2K5C8L5-Aotus,A0A2K5I649-Colobus,A0A2K5LI03-Cercocebus,A0A2K5TT48-Macaca,A0A2K5TT55-Macaca,A0A2K5YLF6-Mandrillus,A0A2K6CIB1-Macaca,A0A2K6LQL2-Rhinopithecus,A0A2K6LQL6-Rhinopithecus,A0A2K6SSG6-Saimiri,A0A2K6SSH3-Saimiri,A0A2R9A537-Pan,A0A2R9AAD3-Pan,F7C5C0-Callithrix,G1QR46-Nomascus,G3QL96-Gorilla,H2PFM6-Pongo,H2QQY4-Pan,Q13216-Homo
