Total UniProts in cluster 64
Total Eukaryotic 64
Total with exons 41
Organisms Pongo,Mandrillus,Rhinopithecus,Pan,Cebus,Callithrix,Macaca,Saimiri,Chlorocebus,Homo,Colobus,Gorilla,Papio,Cercocebus
Organism details A0A096MVJ9-Papio,A0A0D9RU37-Chlorocebus,A0A2I2Z1W9-Gorilla,A0A2I3MLV6-Papio,A0A2I3N177-Papio,A0A2I3RYF9-Pan,A0A2K5IMZ3-Colobus,A0A2K5INH6-Colobus,A0A2K5INW9-Colobus,A0A2K5N7J8-Cercocebus,A0A2K5N7M7-Cercocebus,A0A2K5N7U3-Cercocebus,A0A2K5Q8X8-Cebus,A0A2K5WK39-Macaca,A0A2K5WKE2-Macaca,A0A2K5WKK2-Macaca,A0A2K5Z884-Mandrillus,A0A2K5Z8B7-Mandrillus,A0A2K5Z8L4-Mandrillus,A0A2K6B1P2-Macaca,A0A2K6B1R0-Macaca,A0A2K6B1R5-Macaca,A0A2K6MNL0-Rhinopithecus,A0A2K6MNN4-Rhinopithecus,A0A2K6MNV0-Rhinopithecus,A0A2K6R5V6-Rhinopithecus,A0A2K6R6K2-Rhinopithecus,A0A2K6R6K4-Rhinopithecus,A0A2K6R6L6-Rhinopithecus,A0A2K6R6M1-Rhinopithecus,A0A2K6TT16-Saimiri,A0A2K6TTC6-Saimiri,A0A2R9CMH6-Pan,A0A2R9CP96-Pan,C9JYS6-Homo,F7CBE6-Callithrix,G3QXC5-Gorilla,H2PM35-Pongo,H2QUL1-Pan,P00533-Homo,Q504U8-Homo
