Total UniProts in cluster 26
Total Eukaryotic 26
Total with exons 21
Organisms Pongo,Mandrillus,Nomascus,Rhinopithecus,Pan,Callithrix,Macaca,Papio,Saimiri,Chlorocebus,Homo,Colobus,Gorilla,Cercocebus,Aotus
Organism details A0A096MZS2-Papio,A0A0D9RFW8-Chlorocebus,A0A2K5E812-Aotus,A0A2K5J9L0-Colobus,A0A2K5MC47-Cercocebus,A0A2K5X7V4-Macaca,A0A2K5YEN2-Mandrillus,A0A2K6AR35-Macaca,A0A2K6LTR5-Rhinopithecus,A0A2K6PRA7-Rhinopithecus,A0A2K6PRB7-Rhinopithecus,A0A2K6UBA7-Saimiri,A0A2R8Z9Y0-Pan,A0A2R8Z9Y7-Pan,F5H1U3-Homo,F7IS31-Callithrix,G1QRB2-Nomascus,G3R4L0-Gorilla,H2NG45-Pongo,K7B4Z1-Pan,Q02790-Homo
