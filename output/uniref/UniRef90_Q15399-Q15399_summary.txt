Total UniProts in cluster 78
Total Eukaryotic 78
Total with exons 15
Organisms Pongo,Mandrillus,Nomascus,Pan,Callithrix,Macaca,Homo,Colobus,Papio,Cercocebus
Organism details A0A096MLM5-Papio,A0A2J8TCR8-Pongo,A0A2K5HHD2-Colobus,A0A2K5KYW7-Cercocebus,A0A2K5UN04-Macaca,A0A2K5XTY9-Mandrillus,A0A2K6AP32-Macaca,A0A2R8ZHB2-Pan,D6RA99-Homo,D6RAP2-Homo,D6RCE8-Homo,D6RF68-Homo,F6X7G7-Callithrix,G1S5C6-Nomascus,Q15399-Homo
