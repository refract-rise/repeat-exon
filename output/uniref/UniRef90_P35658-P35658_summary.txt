Total UniProts in cluster 44
Total Eukaryotic 44
Total with exons 26
Organisms Pongo,Mandrillus,Nomascus,Rhinopithecus,Pan,Macaca,Homo,Chlorocebus,Colobus,Gorilla,Papio,Cercocebus
Organism details A0A096P0F3-Papio,A0A0A0MSW3-Homo,A0A0D9RQP3-Chlorocebus,A0A2I3M846-Papio,A0A2I3SSV6-Pan,A0A2K5IGI8-Colobus,A0A2K5IGK7-Colobus,A0A2K5IGM3-Colobus,A0A2K5LTX5-Cercocebus,A0A2K5TQ40-Macaca,A0A2K5TQ60-Macaca,A0A2K5TQ76-Macaca,A0A2K5ZJX9-Mandrillus,A0A2K6E8L2-Macaca,A0A2K6LZC6-Rhinopithecus,A0A2K6PZ01-Rhinopithecus,A0A2K6PZ14-Rhinopithecus,A0A2K6PZ22-Rhinopithecus,A0A2R9B1P9-Pan,A0A2R9B875-Pan,A0A494C1F2-Homo,G1RQA8-Nomascus,G3RLU6-Gorilla,H2PTQ3-Pongo,H2QY24-Pan,P35658-Homo
