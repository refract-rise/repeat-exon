Total UniProts in cluster 27
Total Eukaryotic 27
Total with exons 11
Organisms Pongo,Ursus,Macaca,Homo,Chlorocebus,Saimiri,Gorilla,Aotus
Organism details A0A0D9QUY6-Chlorocebus,A0A2K5F6T3-Aotus,A0A2K6E8A9-Macaca,A0A2K6URD5-Saimiri,A0A452QY57-Ursus,A0A452QYI0-Ursus,A0A452QYK6-Ursus,G3RFL0-Gorilla,G5EA36-Homo,H2NVN8-Pongo,P30260-Homo
