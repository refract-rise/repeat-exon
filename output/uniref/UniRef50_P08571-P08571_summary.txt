Total UniProts in cluster 105
Total Eukaryotic 105
Total with exons 39
Organisms Aotus,Macaca,Papio,Cebus,Mandrillus,Saimiri,Loxodonta,Mustela,Nomascus,Colobus,Pan,Gorilla,Ictidomys,Pongo,Homo,Callithrix,Propithecus,Sus,Chlorocebus,Oryctolagus,Rhinopithecus,Rattus,Mus,Cercocebus
Organism details A0A096MKV9-Papio,A0A0D9RJY7-Chlorocebus,A0A2I3M5H8-Papio,A0A2I3RV91-Pan,A0A2K5ELV7-Aotus,A0A2K5JJ19-Colobus,A0A2K5LKS5-Cercocebus,A0A2K5Q167-Cebus,A0A2K5UK18-Macaca,A0A2K5ZXI0-Mandrillus,A0A2K5ZXL3-Mandrillus,A0A2K6BWC5-Macaca,A0A2K6G5V2-Propithecus,A0A2K6LFL8-Rhinopithecus,A0A2K6LFN2-Rhinopithecus,A0A2K6QJE2-Rhinopithecus,A0A2K6QJF6-Rhinopithecus,A0A2K6UV90-Saimiri,A0A2R9C5W7-Pan,A2SW51-Sus,B3Y6B3-Pan,B3Y6B4-Pan,B3Y6B7-Macaca,B3Y6B8-Macaca,D6RD81-Homo,D6RFL4-Homo,E7EVL5-Homo,F7B9N7-Callithrix,G1RDN0-Nomascus,G1SLB5-Oryctolagus,G3R4C0-Gorilla,G3UH32-Loxodonta,H2PGS5-Pongo,I3MWY3-Ictidomys,M3YAN8-Mustela,P08571-Homo,P10810-Mus,Q4FJP7-Mus,Q63691-Rattus
