Total UniProts in cluster 65
Total Eukaryotic 64
Total with exons 33
Organisms Sus,Macaca,Otolemur,Homo,Papio,Aotus,Pongo,Ailuropoda,Pan,Ursus,Canis,Chlorocebus,Ovis,Gorilla,Rhinopithecus,Cercocebus,Oryctolagus,Mandrillus,Nomascus,Bos,Mustela,Loxodonta,Colobus,Rattus,Saimiri
Organism details A0A096MKZ3-Papio,A0A0D9RSK4-Chlorocebus,A0A2K5F971-Aotus,A0A2K5JY18-Colobus,A0A2K5LGH5-Cercocebus,A0A2K5VEC1-Macaca,A0A2K5YGS9-Mandrillus,A0A2K6BJI3-Macaca,A0A2K6BJI6-Macaca,A0A2K6LI28-Rhinopithecus,A0A2K6QBF5-Rhinopithecus,A0A2K6UW76-Saimiri,A0A2R9CHL6-Pan,A0A384C558-Ursus,A0A452RZL3-Ursus,A0A452RZQ0-Ursus,E2QXA0-Canis,F1S2K0-Sus,F7DXM4-Macaca,G1LYJ4-Ailuropoda,G1QXT2-Nomascus,G1SDP4-Oryctolagus,G3QL93-Gorilla,G3T9N9-Loxodonta,G7P7Q3-Macaca,H0WGB0-Otolemur,H2PFU4-Pongo,H2QR26-Pan,M3Y3A7-Mustela,Q2KI79-Bos,Q6Q287-Rattus,Q9H9E1-Homo,W5P4A0-Ovis
