Total UniProts in cluster 22
Total Eukaryotic 22
Total with exons 14
Organisms Pongo,Nomascus,Pan,Homo,Gorilla
Organism details A0A2I2Z999-Gorilla,A0A2I2ZBP8-Gorilla,A0A2I3RRJ0-Pan,A0A2I3TKS4-Pan,A0A2R9C191-Pan,A0A2R9C1C5-Pan,A0A2R9CBH8-Pan,B0QY07-Homo,G1RXB4-Nomascus,G3RLY4-Gorilla,H2P494-Pongo,H2QLL0-Pan,P32927-Homo,Q6NSJ8-Homo
