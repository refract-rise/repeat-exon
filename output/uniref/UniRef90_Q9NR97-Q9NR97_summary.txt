Total UniProts in cluster 64
Total Eukaryotic 30
Total with exons 26
Organisms Nomascus,Mandrillus,Rhinopithecus,Pan,Cebus,Macaca,Chlorocebus,Colobus,Gorilla,Papio,Aotus,Cercocebus
Organism details A0A096N389-Papio,A0A0D9SDU0-Chlorocebus,A0A2I2Y6J7-Gorilla,A0A2I3H3S2-Nomascus,A0A2I3M3K8-Papio,A0A2I3NB27-Papio,A0A2I3RDS7-Pan,A0A2K5DWU4-Aotus,A0A2K5DWU6-Aotus,A0A2K5HYW8-Colobus,A0A2K5P153-Cercocebus,A0A2K5P193-Cercocebus,A0A2K5P1E3-Cercocebus,A0A2K5Q967-Cebus,A0A2K5Q993-Cebus,A0A2K5TZK9-Macaca,A0A2K5TZL6-Macaca,A0A2K5ZF93-Mandrillus,A0A2K5ZFA6-Mandrillus,A0A2K5ZFB6-Mandrillus,A0A2K6AVR3-Macaca,A0A2K6AVS0-Macaca,A0A2K6AVS7-Macaca,A0A2K6LKE8-Rhinopithecus,A0A2K6LKH5-Rhinopithecus,A0A2K6LKJ6-Rhinopithecus
