Total UniProts in cluster 16
Total Eukaryotic 16
Total with exons 10
Organisms Myotis,Loxodonta,Otolemur,Rattus,Ictidomys,Mus
Organism details D3YW57-Mus,D3YXM2-Mus,G1P060-Myotis,G3U077-Loxodonta,G3UX53-Mus,H0WLS7-Otolemur,I3MBC4-Ictidomys,O89053-Mus,Q3U1N0-Mus,Q91ZN1-Rattus
