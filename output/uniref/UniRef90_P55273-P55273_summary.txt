Total UniProts in cluster 11
Total Eukaryotic 11
Total with exons 9
Organisms Pongo,Nomascus,Pan,Propithecus,Cebus,Callithrix,Otolemur,Homo,Aotus
Organism details A0A2J8LWA9-Pan,A0A2K5DWR1-Aotus,A0A2K5RIQ3-Cebus,A0A2K6FHY5-Propithecus,F7GT81-Callithrix,G1RNT0-Nomascus,H0XKT8-Otolemur,H2NXJ9-Pongo,P55273-Homo
