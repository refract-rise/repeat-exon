Total UniProts in cluster 48
Total Eukaryotic 48
Total with exons 23
Organisms Loxodonta,Gorilla,Ursus,Cebus,Macaca,Otolemur,Rattus,Ailuropoda,Propithecus,Mustela,Bos,Sus,Aotus,Mus,Capra
Organism details A0A0G2K9E0-Rattus,A0A1B0GRG4-Mus,A0A1B0GRY7-Mus,A0A1B0GSG5-Mus,A0A2I2ZKU1-Gorilla,A0A2K5C368-Aotus,A0A2K5CEM1-Aotus,A0A2K5RKB7-Cebus,A0A2K6FGK9-Propithecus,A0A452ERC8-Capra,A0A452STY7-Ursus,A0A452TG61-Ursus,A0A4X1W0C6-Sus,A0A5F8AF79-Macaca,A0A5S6HM99-Sus,A0A5S6IIV1-Sus,E2RUH2-Rattus,G1LVD8-Ailuropoda,G3SPV7-Loxodonta,H0X4W2-Otolemur,M3YBG8-Mustela,Q3SZN8-Bos,Q91VI7-Mus
