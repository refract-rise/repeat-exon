Total UniProts in cluster 55
Total Eukaryotic 55
Total with exons 31
Organisms Sus,Callithrix,Macaca,Otolemur,Homo,Papio,Pongo,Pan,Ursus,Canis,Chlorocebus,Ovis,Gorilla,Rhinopithecus,Felis,Capra,Ictidomys,Cercocebus,Oryctolagus,Mandrillus,Bos,Propithecus,Mustela,Colobus,Equus,Saimiri
Organism details A0A096NXT9-Papio,A0A0D9RMT4-Chlorocebus,A0A2I2UM37-Felis,A0A2K5IMY0-Colobus,A0A2K5KQJ5-Cercocebus,A0A2K5X3D3-Macaca,A0A2K5XIV3-Mandrillus,A0A2K6B2S7-Macaca,A0A2K6GV14-Propithecus,A0A2K6R4X4-Rhinopithecus,A0A2K6UW92-Saimiri,A0A2R9BVA9-Pan,A0A452E7P5-Capra,A0A452SLL8-Ursus,A0A4X1VJZ1-Sus,A7Z024-Bos,E2RK13-Canis,F6RL28-Equus,F6ZU00-Callithrix,G1SPD4-Oryctolagus,G3QUR9-Gorilla,G7NT32-Macaca,H0XV27-Otolemur,H2PVE9-Pongo,I3L855-Sus,I3MDP6-Ictidomys,K7AWF4-Pan,M3YNT8-Mustela,O75695-Homo,Q58DV1-Bos,W5PX03-Ovis
