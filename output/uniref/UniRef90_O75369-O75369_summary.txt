Total UniProts in cluster 31
Total Eukaryotic 31
Total with exons 17
Organisms Pongo,Gorilla,Pan,Homo
Organism details A0A0C4DGA1-Homo,A0A2I2YDG4-Gorilla,A0A2I2YFI5-Gorilla,A0A2I3T3J0-Pan,A0A2I3TG73-Pan,A0A2R9AD40-Pan,A0A2R9ADY8-Pan,A0A2R9AI11-Pan,A0A2R9AJ93-Pan,E7EN95-Homo,G3R3J2-Gorilla,G3RYC3-Gorilla,H2PAE8-Pongo,H2R1I3-Pan,K7CD41-Pan,K7EUV5-Pongo,O75369-Homo
