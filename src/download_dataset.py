import requests
import json
import os
import shutil
import sys
import seaborn as sns
import matplotlib.pyplot as plt
import numpy

from uniprot_exon_data import download_exon
from plot_exon_matrix import plot_matrix
from test_regularity import get_scores
from pdb_uniprot_mapping import map_accession


def download(url):
    r = requests.get(url, headers={"Accept" : "application/json"})
    if not r.ok:
        # r.raise_for_status()
        return None
    else:
        return r.text


def get_taxonomy(uid):
    url = "https://www.ebi.ac.uk/proteins/api/proteins/" + uid
    data = download(url)
    if data:
        js = json.loads(data)
        taxa = js["organism"]["lineage"][0]
        return taxa
    else:
        return None


# all repeatsdb entries
def download_repeatsdb(folder=''):
    service = "http://repeatsdb.bio.unipd.it/ws/search?" + \
              "query=average_unit:1TO9999999999&entry_type=uniprot_protein&collection=uniprot_protein&show=ALL"
    data = json.loads(download(service))
    # save
    dest = folder + 'UniProt_repeatsdb.json'
    if len(data):
        print(len(data), 'entries')
        with open(dest, 'w+') as outfl:
            json.dump(data, outfl)
    return data


folder = 'data/dataset/'
dataname = 'UniProt_repeatsdb.json'


# DOWNLOAD
def phase1():
    download_repeatsdb(folder=folder)


# PROCESSING
def phase2(ulist, name, folder, download=True):
    usummary, eukarya, withexons = open("output/"+name, "w+"), [], []
    # alreadydone = open("donelist.txt").readlines()
    # donelist = open("donelist.txt", "a")
    usummary.write("type\tnumber\n")
    usummary.write("all\t" + str(len(set([u["uniprotid"] for u in ulist])))+ "\n")
    for u in ulist:
        acc = u["uniprotid"]
        # remove titin, always too big
        if acc != 'Q8WZ42': # and acc not in alreadydone:
            # print(acc, get_taxonomy(acc))
            taxa = get_taxonomy(acc)
            if taxa == "Eukaryota":
                eukarya.append(acc)
                if acc not in os.listdir(folder):
                    os.mkdir(folder+acc)
                # first to check: has exons
                if download_exon(acc, folder+acc+'/', save=True):
                    withexons.append(acc)
                    if name != "nonrepeat_dataset_summary.tab":
                        try:
                            res = plot_matrix(acc, plotscores=True, download=True, folder=folder+acc+'/')
                            # print(acc, 'Info:', 'Successfully calculated')
                            if not res:
                                print(acc, 'Error:', 'No plot matrix')
                                shutil.rmtree(folder + acc)
                        except:
                            # if not successful
                            print(acc, 'Error:', sys.exc_info()[0])
                            shutil.rmtree(folder+acc)
                else:
                    # print(acc, 'Error:', 'No exon data available through ENSEMBL REST APIs')
                    shutil.rmtree(folder + acc)
        # donelist.write(acc+'\n')
    usummary.write("eukarya\t" + str(len(set(eukarya))) + "\n")
    usummary.write("withexons\t" + str(len(set(withexons))))
    usummary.close()
    # donelist.close()


# ANALYSIS
def phase3():
    # group subclasses and score them
    subclasses_scores = {}
    for u in [fl for fl in os.listdir(folder) if fl != dataname]:
        acc, ufolder = u, folder+u+'/'
        pdbs = [fl.split('_')[1].strip('.png') for fl in os.listdir(ufolder) if '.png' in fl]
        results = {}
        for p in pdbs:  # list pdbs
            # repdb file
            repdb = json.loads(open(ufolder + "/{}_repeatsdb.json".format(p)).read())
            # matrix data file
            js = json.loads(open(ufolder + "/{}_{}_matrix_data.json".format(acc, p)).read())
            units = js['units']['units']
            exons = js['exons']['units']
            units_reg_assignment = [reg['region_id'][0] for reg in repdb for i in range(len(reg["units"]))]
            for reg in repdb:
                filtered_units = [units[i] for i in range(len(units)) if units_reg_assignment[i] == reg['region_id'][0]]
                scores = get_scores(exons, filtered_units)
                if scores:
                    if reg["classification"][0] in results:
                        results[reg["classification"][0]].append(scores)
                    else:
                        results[reg["classification"][0]] = [scores]
        # only the average of all pdbs of a single protein goes to summary scores
        for cl in results:
            summary_object = {k:numpy.mean([r[k] for r in results[cl]]) for k in results[cl][0]}
            if cl in subclasses_scores:
                subclasses_scores[cl].append(summary_object)
            else:
                subclasses_scores[cl] = [summary_object]
    # summary plots
    lengths_number, exons_number = [], []
    for sub in subclasses_scores:
        scores, lengths = [s["exons"] for s in subclasses_scores[sub]], [s["exons_length_avg"] for s in subclasses_scores[sub]]
        lengths_number.extend([s["exons_lengths"] for s in subclasses_scores[sub]])
        exons_number.extend([s['exons_number'] for s in subclasses_scores[sub]])
        # check number of scores
        if len(scores) > 10:
            print(sub, '|', 'shift:', numpy.mean(scores), 'length:', numpy.mean(lengths))
            plt.clf()
            sns.distplot(scores, bins=20, kde=False)
            plt.title(sub)
            plt.savefig('output/scores_'+sub+'.png')
            plt.clf()
            sns.distplot(lengths, bins=20, kde=False)
            plt.title(sub)
            plt.savefig('output/lengths_' + sub + '.png')
    plt.clf()
    sns.distplot(lengths_number, bins=20, kde=False)
    plt.title('Number of exon lengths')
    print(numpy.mean(exons_number), "mean number of exons in all proteins")
    plt.savefig('output/lengths_number.png')


# dataset of non-repeats
def non_repeat_dataset():
    def first_conversion():
        fl = open("data/non_repeats_PDBs.txt").readlines()
        outfl, outlist = open("data/non_repeats.json", "w+"), []
        for pdb in fl:
            if map_accession(pdb[0:4]):
                uniprots = json.loads(map_accession(pdb[0:4]))[pdb[0:4]]["UniProt"]
                for u in uniprots:
                    mapping = [m for m in uniprots[u]['mappings'] if m['chain_id'] == pdb.strip('\n')[-1]]
                    if mapping:
                        myu = {
                            'pdb_id_chain': pdb.strip("\n"),
                            'uniprotid': u,
                            'mapping': mapping
                        }
                        outlist.append(myu)
        json.dump(outlist, outfl)
    first_conversion()
    ulist = json.loads(open("data/non_repeats.json").read())
    phase2(ulist, "nonrepeat_dataset_summary.tab", "data/non_repeats/")

# phase1()
# # 1468 entries
# ulist = json.loads(open(folder+dataname).read())
# phase2(ulist, "dataset_summary.tab", download=False)
# phase3()

non_repeat_dataset()
