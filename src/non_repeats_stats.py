import os
import json
import numpy
import seaborn as sns
import matplotlib.pyplot as plt


def plothistograms(data, title):
    # Plot seaborn
    kwargs = dict(hist_kws={'alpha': .6}, norm_hist=True, kde=True)
    plt.figure(figsize=(10, 7), dpi=300)
    colors = ["dodgerblue", "orange", "deeppink"]
    for k in data:
        sns.distplot(data[k], color=colors[list(data.keys()).index(k)], label=k, bins=100, **kwargs)
    plt.xlim(0, 300)
    plt.legend()
    plt.title(title)
    plt.savefig(title)
    # # Two plots]
    # colors = ["dodgerblue", "orange", "deeppink"]
    # for d in data:
    #     plt.hist(data[d], color=colors[list(data.keys()).index(d)], label=d, density=True, bins=100, alpha=0.5)
    # plt.legend()
    # plt.savefig(title)
    # plt.close()


def parse_exons(datafolder, filter=False):
    lengths = []
    ulist = {u["uniprotid"]:u for u in json.loads(open("data/non_repeats.json").read())}
    for u in os.listdir(datafolder):
        if u != "UniProt_repeatsdb.json":
            if filter:
                if 'dataset' in datafolder:
                    repeatsfiles = [f for f in os.listdir(datafolder + u + "/") if 'matrix_data' in f]
                    emin, emax = float('inf'), float('-inf')
                    for f in repeatsfiles:
                        for e in json.loads(open(datafolder + u + "/"+f).read())["exons"]["units"]:
                            if e[0] < emin:
                                emin = e[0]
                            if e[-1] > emax:
                                emax = e[-1]
                    if emin != float('inf') and emax != float('-inf'):
                        l = numpy.std([int(e["end"]) - int(e["start"])
                               for e in json.loads(open(datafolder + u + "/" + u + "_exons.json").read())
                               if set(range(int(e["start"]), int(e["end"]))) & set(range(emin, emax))
                             and e['isoform']=='1'])
                if 'non_repeats' in datafolder:
                    # select only regions of non repeated pdbs
                    map = {s for m in ulist[u]['mapping'] for s in set(range(m["unp_start"], m["unp_end"]))}
                    l = numpy.std([int(e["end"]) - int(e["start"])
                         for e in json.loads(open(datafolder + u + "/" + u + "_exons.json").read())
                         if set(range(int(e["start"]), int(e["end"]))) & map and e['isoform']=='1'])
            else:
                l = numpy.std([int(e["end"]) - int(e["start"])
                                for e in json.loads(open(datafolder + u + "/" + u + "_exons.json").read())])
            lengths.append(l)
    return lengths


r, n = parse_exons("data/dataset/", filter=True), parse_exons("data/non_repeats/", filter=True)
data = {"Non_repeats": r,
        "Repeats": n}
print(len(r), "repeated proteins, mean:", numpy.mean(data["Repeats"]), ", standard deviation:",
      numpy.mean([numpy.std(u) for u in n]))
print(len(n), "non repeated proteins, mean:", numpy.mean(data["Non_repeats"]), ", standard deviation:",
      numpy.mean([numpy.std(u) for u in r]))
plothistograms(data, "repeats_nonrepeats_stds")
