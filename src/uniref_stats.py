import urllib.parse
import urllib.request
import requests
import json
from uniprot_exon_data import download_exon
from tools.multi_alignment_plotter import MsaPlotter
import os
# Import pairwise2 module
from Bio import pairwise2
from Bio.Align.Applications import ClustalOmegaCommandline
from Bio import AlignIO
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as ticker
from unidip import UniDip
import unidip.dip as dip
from scipy import stats

folder = '/mnt/ext-1tb/lisanna-old/Projects/TRP-analysis/repeats_evolution_mapping/data/uniref/'
outfolder = "/mnt/ext-1tb/lisanna-old/Projects/TRP-analysis/repeats_evolution_mapping/output/uniref/"
provided = True
# calculate data for all unirefs in the dataset
datafolder = '/mnt/ext-1tb/lisanna-old/Projects/TRP-analysis/repeats_evolution_mapping/data/dataset/'
# combine with one random pdb
#example_ids = [[f.strip(".png") for f in os.listdir(datafolder+u) if '.png' in f][0] for u in os.listdir(datafolder)
#               if u != 'UniProt_repeatsdb.json' and '.png' in ''.join(os.listdir(datafolder+u))]
# print(example_ids)
# or just paper examples
example_ids = ["Q91VI7_3tsrE", "O75473_4bstB", "P16157_1n11A", "Q6PFX9_3utmA", "Q14671_3bsbB", "O14980_4bsmA",
               "O95198_2xn4A", "Q2YDS1_3ei2B", "Q12834_4ggaA", "P18754_1a12A"]
# example_ids = ["Q14671_3bsbB"]
threshold = '50'
organisms_selected = ['Mus', 'Felis', 'Homo', 'Bos']


# TODO: apply organisms_selected filter


def downloadmapping(query, fromid, toid):
    url = "https://www.uniprot.org/uploadlists/"
    params = {
        "from": fromid,
        "to": toid,
        "format": "tab",
        "query": query
    }
    data = urllib.parse.urlencode(params)
    data = data.encode("utf-8")
    req = urllib.request.Request(url, data)
    with urllib.request.urlopen(req) as f:
        response = f.read()
    resp = response.decode("utf-8")
    with open("/mnt/ext-1tb/lisanna-old/Projects/TRP-analysis/repeats_evolution_mapping/output/uniprot_uniref.tab",
              "w+") as outfl:
        outfl.write(resp)
        outfl.close()
    return resp


def download(url):
    r = requests.get(url, headers={"Accept": "application/json"})
    if not r.ok:
        # r.raise_for_status()
        return None
    else:
        return r.text


def get_taxonomy(uid):
    url = "https://www.ebi.ac.uk/proteins/api/proteins/" + uid
    data = download(url)
    if data:
        js = json.loads(data)
        taxa = js["organism"]["lineage"][0]
        return (js["organism"]["lineage"][-1], taxa)
    else:
        return None


def get_sequence(uid):
    url = "https://www.ebi.ac.uk/proteins/api/features/" + uid
    data = download(url)
    if data:
        js = json.loads(data)
        return (js["sequence"])
    else:
        return None


def map_repeats_on_uniprot(repdbid):
    def mapunitindex(pdbindex, pdb_to_pdb_string, pdb_to_uniprot):
        return pdb_to_uniprot[pdb_to_pdb_string.index(pdbindex)]

    # repeat annotation
    url_mappings = "http://repeatsdb.bio.unipd.it/ws/search?query=id:{}" \
                   "&show=ALL&collection=repeat_region".format(repdbid)
    repeats = [r["units"] for r in json.loads(download(url_mappings))]
    # mapping to uniprot
    url_mappings = "http://repeatsdb.bio.unipd.it/ws/search?query=id:{}&show=seq_uniprot,pdb_to_uniprot," \
                   "pdb_to_pdb_string&collection=pdb_chain".format(repdbid)
    js = json.loads(download(url_mappings))[0]
    repeats_mapped = [[(mapunitindex(str(u[0]), js["pdb_to_pdb_string"], js["pdb_to_uniprot"]),
                        mapunitindex(str(u[1]), js["pdb_to_pdb_string"], js["pdb_to_uniprot"])) for u in r] for r in
                      repeats]
    return repeats_mapped


def align_uniprot_sequences(uni1, uni2):
    url = "https://www.ebi.ac.uk/proteins/api/proteins/"
    uni1seq = json.loads(download(url + uni1))["sequence"]["sequence"]
    uni2seq = json.loads(download(url + uni2))["sequence"]["sequence"]
    alignments = pairwise2.align.globalms(uni1seq, uni2seq, 2, -1, -0.5, -0.1)
    # map seq 2 on seq 1
    mapped_indexes, pointer = [], 0
    for pos in range(len(uni2seq)):
        pointer += 1
        if uni2seq[pos] != "-":
            mapped_indexes.append(pointer)
    return mapped_indexes


def mapsequence(uidref, uid):
    rdbid = [i.split("_")[1] for i in example_ids if uidref in i][0]
    repeats = map_repeats_on_uniprot(rdbid)
    mapped = align_uniprot_sequences(uidref, uid)
    repeats_target = []
    for r in repeats:
        for u in r:
            try:
                repeats_target.append((mapped.index(u[0]), mapped.index(u[1])))
            except:
                continue
    return repeats_target


def getdataUniref():
    query = " ".join([i.split("_")[0] for i in example_ids])
    print('query', query)
    refs_to_uni = {ln.split("\t")[1]: ln.split("\t")[0] for ln in
                   downloadmapping(query, "ACC+ID", "NF" + threshold).split("\n")[1:] if ln != ""}
    ufcodes = refs_to_uni.keys()
    refs_to_unis, final_stats = {u: [] for u in ufcodes}, {u: {} for u in ufcodes}
    print('refs', refs_to_unis)
    for ln in downloadmapping(" ".join(ufcodes), "NF" + threshold, "ACC").split("\n")[1:]:
        if ln != "":
            refs_to_unis[ln.split("\t")[0]].append(ln.split("\t")[1])
    # start analysis
    # limit to eukaryotic and annotated with exons
    for r in refs_to_unis:
        print('data for:', r)
        try:
            if r not in os.listdir(folder):
                os.mkdir(folder + r)
            uniprots = refs_to_unis[r]
            # print('uniprots', uniprots)
            euk_unis, exon_unis, organisms, organisms_detail = [], [], set(), {}
            for acc in uniprots:
                taxonomy = get_taxonomy(acc)
                if taxonomy and taxonomy[1] == "Eukaryota":
                    euk_unis.append(acc)
                    if provided:
                        if os.path.isfile(folder + r + '/' + acc + "_exons.json"):
                            ex = json.loads(open(folder + r + '/' + acc + "_exons.json").read())
                        else:
                            ex = download_exon(acc, dest=folder + r + '/', save=True)
                    else:
                        ex = download_exon(acc, dest=folder + r + '/', save=True)
                    if ex:
                        organisms.add(taxonomy[0])
                        organisms_detail[acc] = taxonomy[0]
                        exon_unis.append(acc)
                        # dataset is complete, plot histogram of lengths and number of exons
                        # in correspondence to repeat region
                        # if acc != refs_to_uni[r]:
                        repeats = mapsequence(refs_to_uni[r], acc)
                        exons = [(int(e['start']), int(e['end'])) for e in ex]
                        exon_overlapping = [e for e in exons
                                            if len([r for r in repeats
                                                    if set(range(r[0] + 1, r[1] + 1)) & set(range(e[0], e[1]))])]
                        final_stats[r][acc] = {
                            "sequence": get_sequence(acc),
                            "repeats": repeats,
                            "exons_including_nonrepeats": exons,
                            "exons": exon_overlapping
                        }
                        with open('{}{}-{}.json'.format(outfolder, r, refs_to_uni[r]), 'w') as outfile:
                            json.dump(final_stats[r][acc], outfile)
                            outfile.close()
                    else:
                        continue
            final_stats[r]['summary'] = {
                'total': len(uniprots),
                'eukaryotic': len(euk_unis),
                'with_exons': len(exon_unis)
            }
        except:
            print('ERROR in retrieving data for:', r)
        with open('{}{}-{}_summary.txt'.format(outfolder, r, refs_to_uni[r]), 'w') as outfile:
            outfile.write(" ".join(["Total UniProts in cluster", str(len(refs_to_unis[r]))]) + "\n")
            outfile.write(" ".join(["Total Eukaryotic", str(len(euk_unis))]) + "\n")
            outfile.write(" ".join(["Total with exons", str(len(exon_unis))]) + "\n")
            outfile.write(" ".join(["Organisms", ",".join(organisms)]) + "\n")
            outfile.write(" ".join(
                ["Organism details", ",".join([k + '-' + organisms_detail[k] for k in organisms_detail])]) + "\n")
    with open(outfolder + 'uniref' + threshold + '_summary.json', 'w') as outfile:
        json.dump(final_stats, outfile)
        outfile.close()


def plothistograms(data, title):
    fig, axs = plt.subplots(nrows=2)
    fig.subplots_adjust(hspace=0.45, wspace=0.3)
    units, exons = data["Units"], data["Exons"]
    maxy = 0.5
    yticks = np.arange(0, maxy, float(maxy) / 10)
    maxx = max(units + exons)
    if maxx > 50:
        maxx = 50
    xticks = np.arange(0, maxx + 5, 5)
    xrange = (0, maxx + 5)
    for ax in axs:
        ax.set_yticks(yticks)
        ax.yaxis.set_major_formatter(ticker.PercentFormatter(xmax=1))
        ax.set_ylim((0, maxy))
        ax.set_xticks(xticks)
        ax.set_xlim(xrange)
        ax.set_ylabel('Percentage')
        ax.set_xlabel(title.split("_")[2].strip(".png"))
        # ax.grid(True)
    # units
    axs[0].hist(units, density=True)
    axs[0].set_xticks([])  # overwrite the x-label
    axs[0].set_title('Units')
    # exons
    axs[1].hist(exons, density=True)
    axs[1].set_title('Exons')
    plt.savefig(title)
    plt.close()


def plotstats():
    unirefs = json.loads(open(outfolder + 'uniref' + threshold + '_summary.json').read())
    outputstats = open(outfolder + 'uniref' + threshold + '_summary_stats.txt', 'w+')
    for u in unirefs:
        lengths = {"Units": [len(range(un[0], un[1])) for acc in unirefs[u] for un in unirefs[u][acc]["repeats"]],
                   "Exons": [len(range(un[0], un[1])) for acc in unirefs[u] for un in unirefs[u][acc]["exons"]]}
        number = {"Units": [len(unirefs[u][acc]["repeats"]) for acc in unirefs[u]],
                  "Exons": [len(unirefs[u][acc]["exons"]) for acc in unirefs[u]]}
        # check multimodal features
        data = lengths['Exons']
        data = np.msort(data)
        intervals = UniDip(data).run()
        if lengths['Exons'] and lengths['Units']:
            outputstats.write(u + '\n')
            outputstats.write('\t'.join(['Unimodality test', str(dip.diptst(data)), str(intervals), '\n']))
            # outputstats.write('\t'.join([str(lengths['Exons']), '\n', str(lengths['Units']), '\n']))
            outputstats.write('\t'.join(['Two-Sample Test, lengths',
                                         str(stats.ks_2samp(lengths['Exons'], lengths['Units'])), '\n']))
            outputstats.write('\t'.join(['Two-Sample Test, number',
                                         str(stats.ks_2samp(number['Exons'], number['Units'])), '\n']))
            plothistograms(lengths, "{}{}_Lengths.png".format(outfolder, u))
            plothistograms(number, "{}{}_Lengths.png".format(outfolder, u))


def get_alignments():
    unirefs = json.loads(open(outfolder + 'uniref' + threshold + '_summary.json').read())
    for u in unirefs:
        #if not os.path.isfile('{}{}/{}.fas'.format(folder, u, u)) or not os.path.isfile('{}{}/{}.fas'.format(folder, u, u)):
        # get protein sequences
        sequences = {acc: get_sequence(acc) for acc in unirefs[u]}
        outfasta = open('{}{}/{}.fas'.format(folder, u, u), 'w+')
        outfasta.write('\n'.join(['> ' + s + '\n' + sequences[s] for s in sequences if sequences[s]]))
        outfasta.close()
        # run only if multiple sequence
        if len(sequences.keys()) > 1:
            clustalomega_cline = ClustalOmegaCommandline(infile='{}{}/{}.fas'.format(folder, u, u),
                                                     outfile='{}{}/{}.aln'.format(folder, u, u),
                                                     outfmt='clu',
                                                     verbose=True, auto=True, force=True)
            stdout, stderr = clustalomega_cline()
        print(u, "checked")
    return unirefs


def plotalignments():
    unirefs = get_alignments()  # comment if already calculated
    unirefs = json.loads(open(outfolder + 'uniref' + threshold + '_summary.json').read())
    for u in unirefs:
        # print('data/uniref/{}/{}.aln'.format(u, u))
        if os.path.isfile('{}{}/{}.aln'.format(folder, u, u)):
            regions = {acc: [e for e in unirefs[u][acc]["exons"]] for acc in unirefs[u]}
            # regions = {'Q3SZN8': [(454,456)], 'A0A2K6FGK9': [(372,375), (380,382)]}
            MsaPlotter().plot(align='{}{}/{}.aln'.format(folder, u, u), formats='aln', types='pep',
                              regions=regions, outfl='{}{}'.format(outfolder, u), adjust_height=True)
            print('{}{}'.format(outfolder + u))
            print(u, "done")
        else:
            print(u, "alignment not found")


def checkOrganisms():
    folders = [fl for fl in os.listdir(outfolder) if 'UniRef' + threshold in fl and '.txt' in fl]
    organisms, organisms_sets = [], []
    for fid in folders:
        orgs = open(outfolder + fid).readlines()[3].split(" ")[1].strip("\n").split(",")
        for org in orgs:
            organisms.append(org)
        organisms_sets.append(set(orgs))
    print(sorted({x: organisms.count(x) for x in set(organisms)}.items(), key=lambda kv: kv[1]))
    print(set.intersection(*organisms_sets))


def centromeric_analysis():
    def remapseq(seq):
        index, indexes = 1, []
        for i in seq:
            if i != '-':
                indexes.append(index)
                index += 1
            else:
                indexes.append(0)
        return indexes

    def getoverlap(set1, set2):
        ovl = 2 * len(set1 & set2) / (len(set1) + len(set2))
        return ovl if ovl != 0 else np.nan

    def getinternalmatrix(seq1, seq2, exons_proteins):
        s1s2matrix = np.matrix([[getoverlap(set(range(ex1[0], ex1[1])), set(range(ex2[0], ex2[1])))
                                 for ex1 in exons_proteins[seq1] if ex1] for ex2 in exons_proteins[seq2] if ex2])
        return s1s2matrix

    # unirefs = get_alignments()  # comment if already calculated
    output = open(outfolder + "uniref{}_summary_centroidscores.txt".format(threshold), "w+")
    summary = json.loads(open(outfolder + "/uniref{}_summary.json".format(threshold)).read())
    for cl in summary:
        flname = '{}{}/{}.aln'.format(folder, cl, cl)
        if os.path.isfile(flname):
            alignment = AlignIO.read(flname, "clustal")
            # for record in alignment: record.id & record.seq
            aligned_sequences = {record.id: record.seq for record in alignment}
            exons_proteins = {u: summary[cl][u]["exons"] for u in summary[cl] if u != "summary"}
            commonkeys = set(exons_proteins.keys()) & set(aligned_sequences.keys())
            # exons are mapped to the uniprot sequence without alignment gaps
            # remap exons on global alignment
            aligned_sequences_exons = {record:
                                           [[remapseq(aligned_sequences[record]).index(e[0]) + 1,
                                             remapseq(aligned_sequences[record]).index(e[1]) + 1] for e in
                                            exons_proteins[record]]
                                       for record in commonkeys
                                       if exons_proteins[record] and exons_proteins[record][-1][1] <= len(
                    str(aligned_sequences[record]).replace('-', ''))}
            # one matrix for each couple of sequences, then one unique matrix for the cluster
            clustermatrix = [[np.nanmean(getinternalmatrix(seq1, seq2, aligned_sequences_exons))
                              for seq1 in aligned_sequences_exons] for seq2 in aligned_sequences_exons]
            output.write(' '.join([cl,
                                   str(summary[cl]['summary']['total']) if 'summary' in summary[cl] else '',
                                   str(len(commonkeys)),
                                   str(np.nanmean(clustermatrix))]) + "\n")
        else:
            print(cl, "not found")


# for o in example_ids:
#     exc = "python src/plot_exon_matrix.py {} --pdb {} --folder /home/lisanna-old/Projects/TRP-analysis/" \
#           "repeats_evolution_mapping/data/dataset/{}/".format(o.split("_")[0], o.split("_")[1], o.split("_")[0])
#     os.system(exc)


# getdataUniref()
# checkOrganisms()
# plotstats()
# plotalignments()
centromeric_analysis()

# plothistograms({"Units":[4, 4, 8], "Exons":[4, 8, 8]}, "Myfake_Hist_Try.png")
