import re
import os

numeric_pattern = "[-+]?\d+[\.]?\d*"


def enrich_reg(dest, start, end):
    return [(int(re.match(numeric_pattern, u.split(" ")[0]).group(0)),
             int(re.match(numeric_pattern, u.split(" ")[1]).group(0))) for u in dest if
            int(re.match(numeric_pattern, u.split(" ")[0]).group(0)) >= start and int(
                re.match(numeric_pattern, u.split(" ")[1]).group(0)) <= end]


def repeatsdb_parser(fl_path):

    # instantiate output
    regions, dfl = [], None
    # test if file
    if not os.path.exists(fl_path):
        print("No RepeatsDB file")

    else:

        fl, dfl = open(fl_path).readlines(), {}

        for ln in fl:
            # check if regular line: key, val
            if len(ln.strip().split("\t")) > 1:
                k, v = ln.strip().split("\t")[0], ln.strip().split("\t")[1]
                if k in ["REG", "UNIT", "INS", "SUPREG"]:
                    dfl[k] = [v] if k not in dfl else dfl[k] + [v]
                else:
                    dfl[k] = v

        # deprecated
        # connects = {}
        # if 'SUPREG' in dfl:
        #     for sreg in dfl['SUPREG']:
        #         sreg = sreg.split(" ")
        #         connects[(sreg[0], sreg[1])] = (sreg[2], sreg[3])
        #         connects[(sreg[2], sreg[3])] = (sreg[0], sreg[1])

        if "REG" in dfl:
            for reg in dfl["REG"]:
                # example output:
                # {'units':[(26,48),(49,70),(71,94),(95,119)], 'insertions':[], 'class':'III', 'subclass':'2'}
                final_reg = {"class": reg.split(" ")[2], "subclass": reg.split(" ")[3]}
                if reg.split(" ")[0] != "None":
                    start, end = int(reg.split(" ")[0]), int(reg.split(" ")[1])
                    final_reg['units'] = enrich_reg(dfl["UNIT"], start, end) if 'UNIT' in dfl else []
                    final_reg['insertions'] = enrich_reg(dfl["INS"], start, end) if 'INS' in dfl else []
                    final_reg['superreg'] = []
                    # deprecated
                    '''for c in connects:
                        if (start, end) in connects:
                            final_reg['superreg'].append(connects[(start, end)])'''
                    regions.append(final_reg)
            dfl['regions'] = regions
        del dfl['REG']
        del dfl['UNIT']

    return dfl
