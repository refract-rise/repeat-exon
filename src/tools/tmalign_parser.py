import math
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq


def read_tmalign_as_seqrec_pair(tm_output, ref_id, eqv_id):

    """Create a pair of SeqRecords from TMalign output."""
    lines = tm_output.splitlines()
    # Extract the TM-score (measure of structure similarity)
    # Take the mean of the (two) given TM-scores -- not sure which is reference
    tmscores = []
    for line in lines:
        if line.startswith('TM-score'):
            # TMalign v. 2012/05/07 or earlier
            tmscores.append(float(line.split(None, 2)[1]))
        elif 'TM-score=' in line:
            # TMalign v. 2013/05/11 or so
            tokens = line.split()
            for token in tokens:
                if token.startswith('TM-score='):
                    _key, _val = token.split('=')
                    tmscores.append(float(_val.rstrip(',')))
                    break
    if tmscores:
        tmscore = math.fsum(tmscores) / len(tmscores)
        # Extract the sequence alignment
        lastlines = lines[-5:]
        assert lastlines[0].startswith('(":"') # (":" denotes the residues pairs
        assert not lastlines[-1].strip()
        refseq, eqvseq = lastlines[1].strip(), lastlines[3].strip()
        return (SeqRecord(Seq(refseq), id=ref_id,
                          description="TMalign TM-score=%f" % tmscore),
                SeqRecord(Seq(eqvseq), id=eqv_id,
                          description="TMalign TM-score=%f" % tmscore),
                tmscore)
    else:
        return None