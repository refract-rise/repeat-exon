from ftplib import FTP
import gzip
from xml.dom.minidom import parseString


def download_sifts(pdb, chain, acc, folder=''):
    ftp_folder = pdb[1:3]
    ftp = FTP('ftp.ebi.ac.uk')
    ftp.login()
    ftp.cwd('pub/databases/msd/sifts/split_xml/'+ftp_folder)
    flname = folder+'{}.xml.gz'.format(pdb)
    ftp.retrbinary('RETR {}.xml.gz'.format(pdb), open(flname, 'wb').write)
    return parse_sifts(flname, chain, acc)


def parse_sifts(siftsfile, chain, acc=None):

    def get_one(r, attribute, acc=acc):
        for ref in r.getElementsByTagName("crossRefDb"):
            if ref.getAttribute('dbSource') == attribute:
                if acc and ref.getAttribute('dbAccessionId') == acc:
                    return int(ref.getAttribute('dbResNum'))
                if ref.getAttribute('dbResNum') != "null":
                    return ref.getAttribute('dbResNum')
        return None

    # open and read gzipped xml file
    infile = gzip.open(siftsfile)
    content, references = infile.read(), {}
    # parse xml file content
    dom = parseString(content)
    entities = dom.getElementsByTagName("entity")
    for e in entities:
        if e.getAttribute('entityId') == chain:
            segments = e.getElementsByTagName("segment")
            for s in segments:
                # select necessary references
                refs = {get_one(r, "UniProt", acc=acc): int(r.getAttribute("dbResNum"))
                        for r in s.getElementsByTagName("residue") if get_one(r, "PDB")}
                references.update(refs)
    return references


