import argparse
from Bio.PDB import PDBParser
from Bio.PDB import PDBIO
from Bio.PDB import Select
import requests
import subprocess
import pandas as pd
import json

# relative imports
from tools.repeatsdb_parser import repeatsdb_parser
from tools.tmalign_parser import read_tmalign_as_seqrec_pair


# https://files.rcsb.org/download/4hhb.pdb
def download(url):
    r = requests.get(url, headers={"Accept": "application/json"})
    if not r.ok:
        r.raise_for_status()
        return None
    else:
        return r.text


def download_pdb(pdb):
    service = "https://files.rcsb.org/download/{}.pdb".format(pdb)
    data = download(service)
    if data:
        with open("data/pdb/{}.pdb".format(pdb), "w+") as outfl:
            outfl.write(data)
            outfl.close()
            return True
    else:
        return None


def save_structure(chain, pdbid, u1, destpath='data/pdb/'):
    def resid(res):
        return str(res.get_id()[1]) + res.get_id()[2] if res.get_id()[2] != ' ' else str(res.get_id()[1])

    class ResSelect(Select):
        def accept_residue(self, residue):
            if resid(residue) in rangelist:
                return 1
            else:
                return 0

    io = PDBIO()
    list_residues = [resid(res) for res in chain if res.get_id()[0] == ' ' and not res.is_disordered()]
    # this lists only residues in structure
    io.set_structure(chain)
    # rangelist defines unit residues
    rangelist = [str(i) for i in range(int(u1[0]), int(u1[1]) + 1) if str(i) in list_residues]
    if rangelist:  # external to the structure
        filename = pdbid + '_' + str(u1[0]) + '-' + str(u1[1])
        io.save(destpath + filename + '.pdb', ResSelect())
        return filename
    else:
        print("The exon is not mapped to structure", u1)
        return None


def pdb_for_alignment(pdb, inputfl, start=None, end=None):
    fragments, output = {}, {}
    # parse inputfl
    if '.db' in inputfl:
        # manage repeatsdb file input
        fragments = [[u for u in reg["units"]] for reg in repeatsdb_parser(inputfl)["regions"]]
    elif '_repeatsdb.json' in inputfl:
        # manage repeatsdb json file input
        flcontent = json.loads(open(inputfl).read())
        fragments = [[u for u in reg["units"]] for reg in flcontent]
    elif '.tsv' in inputfl:
        # manage exon file input
        data = pd.read_csv(inputfl, sep='\t')
        # map on pdb, eventually on start and end (repeat region)
        # filter isoform
        isoform1 = data.query('uniprot_isoform == 1')
        starts, ends = isoform1[pdb + '_start'], isoform1[pdb + '_end']
        fragments = [[(starts[i], ends[i])
                      for i in range(len(starts)) if starts[i] != '-' and ends[i] != '-']]
        output['mapping'] = {
            str(isoform1['uniprot_start'][i]) + '-' + str(isoform1['uniprot_end'][i]): str(starts[i]) + '-' + str(
                ends[i])
            for i in range(len(isoform1['uniprot_start']))}
    else:
        print("Unknown file format")
    # biopython inits
    pdb_parser = PDBParser(QUIET=True)
    pdbid, chain = pdb[0:4], pdb[4]
    filenames = []
    if download_pdb(pdbid):
        # Get the structure
        ref_structure = pdb_parser.get_structure("reference", "data/pdb/{}.pdb".format(pdbid))
        chain = ref_structure[0][chain]
        for reg in fragments:
            for u in reg:
                struct = save_structure(chain, pdb, u)
                if struct:
                    filenames.append(struct)
    else:
        print("Download failed")
    output['filenames'] = filenames
    return output


def align_fragments(pdb, inputfl, start=None, end=None):
    data = pdb_for_alignment(pdb, inputfl, start=start, end=end)
    filenames = data['filenames']
    # align each pdb
    done, tmscores = [], {}
    for fl1 in filenames:
        for fl2 in filenames:
            if fl1 != fl2 and (fl1, fl2) not in done and (fl2, fl1) not in done:
                output = subprocess.Popen(
                    "TMalign data/pdb/{}.pdb data/pdb/{}.pdb".format(fl1, fl2), shell=True, stdout=subprocess.PIPE
                ).stdout.read().decode()
                # write output
                open('data/tmalign/' + fl1 + '__' + fl2 + '.txt', 'w+').write(output)
                tm_score = read_tmalign_as_seqrec_pair(output, fl1, fl2)
                if tm_score:
                    done.append((fl1, fl2))
                    tmscores[fl1.split('_')[1] + '_' + fl2.split('_')[1]] = tm_score[-1]
    return tmscores


if __name__ == "__main__":  # execute only if run as a script

    def arg_parser():  # Parser implementation
        parser = argparse.ArgumentParser(prog='pairwise_pdb_alignment.py',
                                         epilog="  Example 1: pairwise_pdb_alignment.py 1gxrA",
                                         formatter_class=argparse.RawDescriptionHelpFormatter)
        parser.add_argument('pdb', help='RepeatsDB identifier, es: 1gxrA')
        parser.add_argument('--fragments', help='Input file path')
        parser.add_argument('--start', help='Index start', default=None)
        parser.add_argument('--end', help='Index end', default=None)
        return parser.parse_args()


    # parse and config arguments
    args = arg_parser()

    print(align_fragments(args.pdb, args.fragments, start=args.start, end=args.end))
