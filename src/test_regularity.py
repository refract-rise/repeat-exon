import argparse
import json
import numpy


def score_overlap(exons, units):

    def getmatch(index, array, what):
        best, score = [], float("inf")
        for u in array:
            s = abs(index-u[0] if what == 'start' else index-u[-1])
            if s < score:
                best, score = u, s
        return best

    # reference array is the first
    matches, scores = {}, []
    for e in exons:
        matchstart, matchend = getmatch(e[0], units, 'start'), getmatch(e[-1], units, 'end')
        scores.append(numpy.mean([abs(e[0] - matchstart[0]), abs(e[-1] - matchend[-1])]))

    # score matches
    return numpy.mean(scores)


def length_clusters(lengths):
    clusters, threshold = [], 3
    while len(lengths) > 0:
        mode = max(set(lengths), key=lengths.count)
        similar_lengths = [e for e in lengths if mode-threshold <= e <= mode+threshold]
        clusters.append(similar_lengths)
        lengths = [l for l in lengths if l not in similar_lengths]
    return clusters


def get_scores(exons, units):
    # exon spans in units
    filtered_exons = [e for e in exons if units[-1][-1] >= e[0] >= units[0][0] or units[0][0] <= e[-1] <= units[-1][-1]]
    # flattenunits = set([i for u in units for i in u])
    # filtered_exons = [e for e in exons if set(e).intersection(flattenunits)]
    if filtered_exons and len(filtered_exons) > 3:
        exon_score = score_overlap(filtered_exons, units)
        unit_score = score_overlap(units, filtered_exons)
        # calculate number of different exon lengths
        return {
            'exons': exon_score,
            'units': unit_score,
            'exons_number': len(filtered_exons),
            'units_number': len(units),
            'exons_lengths': len(length_clusters([len(e) for e in filtered_exons])),
            'exons_length_avg': numpy.mean([len(a) for a in filtered_exons]),
            'units_length_avg': numpy.mean([len(a) for a in units])
        }
    else:
        return None


if __name__ == "__main__":  # execute only if run as a script

    def arg_parser():  # Parser implementation
        parser = argparse.ArgumentParser(prog='test_regularity.py',
                                         epilog="   test_regularity.py O22160 --data data/dataset/O22160/O22160_3n90A_matrix_data.json",
                                         formatter_class=argparse.RawDescriptionHelpFormatter)
        parser.add_argument('uniprot', help='UniProtKB identifier, es: P08631')
        parser.add_argument('--data', help='Summary file path')
        return parser.parse_args()

    # parse and config arguments
    args = arg_parser()
    # get_scores(args.data)

    # test
    exons = [[1, 2, 3, 4, 5, 6], [7, 8], [9, 10], range(11, 30)]
    print(length_clusters([len(e) for e in exons]))

    # units = [[1, 2, 3], [4, 5, 6], [7, 8]]
    # print(score_overlap(exons, units))
    # print(score_overlap(units, exons))