#!/usr/bin/env python

# Launch only with Python 2.7!
# Tell PyMOL to launch quiet (-q), fullscreen (-e) and without internal GUI (-i)
import __main__
__main__.pymol_argv = ['pymol', '-qei']
import pymol

import sys, os
import readline
import json
import argparse



def colorStructure(output, pdbid, chain, array):

    color1, color2 = "ruby", "yelloworange"

    '''

    This script launches PyMOL without any GUI for scripting only.
    It enables tab-completion on the python command line and does the PyMOL environment setup (you need to adjust the moddir variable!).
    Hint: You may save this as "pymol-cli" executable.

    '''

    # autocompletion
    readline.parse_and_bind('tab: complete')

    # pymol environment
    moddir = '/opt/pymol-svn/modules'
    sys.path.insert(0, moddir)
    os.environ['PYMOL_PATH'] = os.path.join(moddir, 'pymol/pymol_path')

    # pymol launching
    pymol.pymol_argv = ['pymol', '-qc'] + sys.argv[1:]

    pymol.finish_launching()
    cmd = pymol.cmd

    # clear figure
    cmd.delete("all")

    # Load Structure
    cmd.fetch(pdbid, "MyStructure")
    cmd.disable("all")
    cmd.enable("MyStructure")

    objects = cmd.get_object_list()
    objectname = objects[-1]
    cmd.set_name(objectname, "MyStructure")
    cmd.select("MyChain", "chain " + chain)
    cmd.set("cartoon_cylindrical_helices", "1")
    cmd.set("cartoon_smooth_loops", "1")
    cmd.set("ray_shadows", "0")
    cmd.orient()
    # color
    color = color1
    for unit in array:
        for index in unit:
            cmd.select('thisresidue', 'resi %s' % (index))
            cmd.color(color, 'thisresidue')
        color = color2 if color == color1 else color1
    cmd.hide("everything", "MyStructure")
    cmd.show_as("cartoon", "MyChain")
    cmd.bg_color("white")
    print('saving png:', output + '.png')
    # ! ray() is required to get png
    cmd.ray()
    cmd.png(output+'_pdb.png', 800, 800)
    print('saving session:', output+'.pse')
    cmd.save(output+'.pse')


########################################################################

address = "/mnt/ext-1tb/lisanna-old/Projects/TRP-analysis/repeats_evolution_mapping/"
folder = address+"figures_paper/"
for rid in ["Q91VI7_3tsrE", "O75473_4bstB", "P16157_1n11A", "Q6PFX9_3utmA", "Q14671_3bsbB",
            "O14980_4bsmA", "O95198_2xn4A", "Q2YDS1_3ei2B", "Q12834_4ggaA", "P18754_1a12A"]:
# for rid in ["Q91VI7_3tsrE"]:
    uniprot, pdb = rid.split('_')[0], rid.split('_')[1]
    pdbid, chain = pdb[0:4], pdb[4]
    output = folder + rid
    regions = json.loads(open(address +
                              "data/dataset/{}/{}_{}_matrix_data.json".format(uniprot, uniprot, pdb)).read())["exons"]["units"]
    colorStructure(output, pdbid, chain, regions)

# if __name__ == "__main__":
#
#     def arg_parser():  # Parser implementation
#         parser = argparse.ArgumentParser(prog='pymol_color.py',
#                                          epilog="   pymol_color.py 3tsrE",
#                                          formatter_class=argparse.RawDescriptionHelpFormatter)
#         parser.add_argument('pdb', help='RepeatsDB identifier, es: 3tsrE')
#         parser.add_argument('--output', help='Output name')
#         return parser.parse_args()
#
#     # parse and config arguments
#     args = arg_parser()

    # test
    # customregions = [
    #     [1, 2, 3, 4, 5],
    #     [6, 7, 8, 9],
    # ]
    # pdbid, chain = args.pdb[0:4], args.pdb[4]
    # regions = customregions
    # output = args.output if args.output else 'myFig'
    # colorStructure(output, pdbid, chain, regions)


