import requests
import json
import argparse


def download(url):
    r = requests.get(url, headers={ "Accept" : "application/json"})
    if not r.ok:
        r.raise_for_status()
        return None
    else:
        return r.text


# http://repeatsdb.bio.unipd.it/ws/search?entry_type=repeat_region&id=1lwuI&collection=repeat_region&show=ALL
def download_repeatsdb(pdb, folder=''):
    service = "http://repeatsdb.bio.unipd.it/ws/search?" + \
              "entry_type=repeat_region&id={}&collection=repeat_region&show=ALL".format(pdb)
    data = json.loads(download(service))
    # save
    dest = folder + pdb + '_repeatsdb.json'
    if len(data):
        with open(dest, 'w+') as outfl:
            json.dump(data, outfl)
    return data


if __name__ == "__main__":
    # execute only if run as a script

    def arg_parser():
        # Parser implementation
        parser = argparse.ArgumentParser(prog='pdb_repeat_data.py',
                                         epilog="  Example 1: pdb_repeat_data.py 1lwuI \n",
                                         formatter_class=argparse.RawDescriptionHelpFormatter)
        parser.add_argument('repdb_id', help='RepeatsDB identifier, es: 1lwuI')

        return parser.parse_args()

    # parse and config arguments
    args = arg_parser()

    download_repeatsdb(args.repdb_id)