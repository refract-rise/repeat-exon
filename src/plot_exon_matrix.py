
import os
import argparse
import json
import numpy as np
import pandas as pd

# relative imports
from tools.repeatsExons_matrix import distance_matrix_from_regions, merge_matrices, draw_matrix, adjust_regions
from pairwise_pdb_alignment import align_fragments
from pdb_uniprot_mapping import uniprot_summary
from pdb_repeat_data import download_repeatsdb
from uniprot_exon_data import uniprot_exons


def getscore(unit1, unit2, alignments):

    id1, id2 = str(unit1[0]) + '-' + str(unit1[-1]), str(unit2[0]) + '-' + str(unit2[-1])
    myid = id1 + '_' + id2
    if myid in alignments:
        return alignments[myid]
    else:
        myrevid = id2 + '_' + id1
        if myrevid in alignments:
            return alignments[myrevid]
        else:
            return 0


def map_pdbs(fl, acc, download=False, folder='', plotscores=True, pdbfilter=None):

    data = pd.read_csv(fl, sep='\t')
    repdbids = set(data.columns[1:])

    if download:
        # download repeats
        for pdb in repdbids:
            download_repeatsdb(pdb, folder=folder)
    repdb_files = [fl.split('_')[0] for fl in os.listdir(folder) if 'repeatsdb' in fl]

    # init output, evaluate superimposition between PDBs
    pdbs_mapped, pdblist = {}, [pdb for pdb in repdbids if pdb in repdb_files]
    if pdbfilter:
        pdblist = [pdbfilter]
    for pdb in pdblist:
        repdb = pdb + '_repeatsdb.json'
        repdb_data = json.loads(open(folder + repdb).read())
        pdb_indexes_range = [i.split(',')[0] for i in data[pdb] if i != '-']
        pdb_indexes_mapped = {
            data[pdb][i].split(',')[0]: int(data[acc][i].split(',')[0])
            for i in range(len(data[acc])) if data[pdb][i] != '-'
        }
        # check that repeated region is in mapped uniprot
        # filter units basing on check, add insertions
        # flatted_insertions = [i for reg in repdb_data
        #                       for ins in reg['insertions']
        #                       for i in pdb_indexes_range[
        #                           pdb_indexes_range.index(str(ins[0])): pdb_indexes_range.index(str(ins[1])) + 1
        #                       ]]
        # filtered_units = [un for reg in repdb_data for un in reg['units']
        #                   if str(un[0]) in pdb_indexes_range and str(un[1]) in pdb_indexes_range]
        # filtered_units_indexes = [
        #     [pdb_indexes_mapped[i]
        #      for i in pdb_indexes_range[pdb_indexes_range.index(str(un[0])):pdb_indexes_range.index(str(un[1])) + 1]
        #      if i not in flatted_insertions]
        #     for un in filtered_units
        #     if str(un[0]) in pdb_indexes_range and str(un[1]) in pdb_indexes_range]
        filtered_units = [un for reg in repdb_data for un in reg['units']
                          if str(un[0]) in pdb_indexes_range and str(un[1]) in pdb_indexes_range]
        filtered_units_indexes = [
            [pdb_indexes_mapped[i]
             for i in pdb_indexes_range[pdb_indexes_range.index(str(un[0])):pdb_indexes_range.index(str(un[1])) + 1]]
            for un in filtered_units
            if str(un[0]) in pdb_indexes_range and str(un[1]) in pdb_indexes_range]
        insertions = [[pdb_indexes_mapped[i]
            for i in pdb_indexes_range[pdb_indexes_range.index(str(ins[0])): pdb_indexes_range.index(str(ins[1])) + 1]]
                              for reg in repdb_data for ins in reg['insertions']
                      if str(ins[0]) in pdb_indexes_range and str(ins[1]) in pdb_indexes_range]
        # units scores
        scores = [[1 for u2 in filtered_units] for u1 in filtered_units]
        if plotscores:
            alignments = align_fragments(pdb, folder+pdb+'_repeatsdb.json')
            # convert filtered units indexes to repdb file
            scores = [[getscore(u1, u2, alignments) for u2 in filtered_units] for u1 in filtered_units]
        pdbs_mapped[pdb] = {
            'units': filtered_units_indexes,
            'scores': scores,
            'indexes': pdb_indexes_mapped,
            'insertions': insertions}
    return pdbs_mapped


def plot_matrix(acc, folder='', zoom=False, plotscores=True, download=False, pdbfilter=None):

    # {'units': [[1,2,..], [..], ..], 'scores': [[0.5,0.1,..], [..], ..]}

    # select one isoform
    # map pdbs
    if download:
        # uniprot based summary
        uniprot_summary(acc, folder=folder)
    pdbs_mapped = map_pdbs(folder + acc + '_summary.tsv', acc, download=download, folder=folder, plotscores=plotscores, pdbfilter=pdbfilter)

    # exons
    if download:
        # create exon tsv
        uniprot_exons(acc, folder=folder, summary=folder + acc + '_summary.tsv')
    data = json.loads(open(folder + acc + '_exons.json').read())
    # avoid having start and end in same index
    exon_data = {
        'units': [[i for i in range(int(data[ei]['start']), int(data[ei]['end']) + 1)]
                  for ei in range(len(data)) if data[ei]['isoform'] == '1'],
        'exon_ids': [data[ei]["id"] for ei in range(len(data)) if data[ei]['isoform'] == '1']
    }

    # init scores
    exon_data['scores'] = [[1 for unis in exon_data['units']] for unis in exon_data['units']]
    if pdbs_mapped:
        # units
        unit_data = {'units': [], 'scores': []}
        for pdb in pdbs_mapped:
            # one plot for PDB
            unit_data['units'] = pdbs_mapped[pdb]['units']
            unit_data['scores'] = pdbs_mapped[pdb]['scores']

            if unit_data['units']:
                # score exon similarity based on the available pdbs
                if plotscores:
                    # try:
                    print([(u[0],u[-1]) for u in exon_data['units']])
                    alignments = align_fragments(pdb, folder + acc + '_exons.tsv')
                    rev_pdb_mapped = {pdbs_mapped[pdb]['indexes'][k]: k for k in pdbs_mapped[pdb]['indexes']}
                    converted_exon_data = [[rev_pdb_mapped[i] if i in rev_pdb_mapped else '-' for i in u] for u in exon_data['units']]
                    scores = [[getscore(u1, u2, alignments) for u2 in converted_exon_data] for u1 in converted_exon_data]
                    # exon_data['scores'].extend(scores)
                    exon_data['scores'] = scores
                    # except:
                    #     print("Error in PDB alignment")

                units = np.array([[u[0], u[-1]+1] for u in unit_data['units']])
                untsim = np.array(unit_data['scores'])
                exons = np.array([[u[0], u[-1]] for u in exon_data['units']])
                # fix exon labels
                exons_fixed = np.array([exons[i]
                               if i == 0 or exons[i][0] == exons[i-1][1]
                               else [exons[i][0]-1, exons[i][1]]
                               for i in range(len(exons))])
                exnsim = np.array(exon_data['scores'])

                insertions = np.array([[u[0], u[-1]+1] for u in pdbs_mapped[pdb]['insertions']])

                # filter exon and units if zoom
                if zoom:
                    start, end = int(zoom.split("-")[0]), int(zoom.split("-")[1])
                    unitstokeep = [ind for ind, u in enumerate(unit_data['units']) if start < u[0] < end or start < u[-1] < end]
                    exonstokeep = [ind for ind, u in enumerate(exon_data['units']) if start < u[0] < end or start < u[-1] < end]
                    units, untsim = units[unitstokeep], untsim[unitstokeep]
                    exons_fixed, exnsim = exons_fixed[exonstokeep], [exnsim[i][exonstokeep] for i in exonstokeep]

                m = max(units[-1][-1], exons_fixed[-1][-1])
                exons = adjust_regions(exons, 3)
                unit_mat = distance_matrix_from_regions(units, m, untsim, (0, 1))
                exon_mat = distance_matrix_from_regions(exons_fixed, m, exnsim, (-1, 0))
                cmat = merge_matrices(exon_mat, unit_mat, m)
                # save data
                clip = True if zoom else False
                with open(folder + acc + '_' + pdb + '_matrix_data.json', 'w+') as outfl:
                    outfl.write(json.dumps({'exons': exon_data, 'units': unit_data}))
                draw_matrix(cmat, units, exons_fixed, rboxes=[exons_fixed], bboxes=[units], bboxes_labels=['Units'],
                            rboxes_labels=['Exons'], edgewidth=0.5, bottom_projections=insertions,
                            cmap_name='PuOr', secondaryticks_top='boundaries',
                            secondaryticks_left="residues", outname=folder + acc + '_' + pdb + '.png', clip=clip)
                # clip to show all exons
            else:
                print("Error: PDB", pdb, "has no units.")
        return True
    else:
        print("Error: no PDBs mapped")
        return False


if __name__ == "__main__":  # execute only if run as a script

    def str2bool(v):
        if v.lower() in ('yes', 'true', 't', 'y', '1'):
            return True
        elif v.lower() in ('no', 'false', 'f', 'n', '0'):
            return False
        else:
            raise argparse.ArgumentTypeError('Boolean value expected.')

    def arg_parser():  # Parser implementation
        parser = argparse.ArgumentParser(prog='uniprot_exon_data.py',
                                         epilog="  Example 1: uniprot_exon_data.py P29373",
                                         formatter_class=argparse.RawDescriptionHelpFormatter)
        parser.add_argument('uniprot', help='UniProtKB identifier, es: P08631')
        parser.add_argument('--folder', help='Working folder'),
        parser.add_argument('--zoom', default=False, help='Zoom figure, start and end es. 4-17'),
        parser.add_argument('--pdb', help='PDB code of structure to plot, filtering out others'),
        parser.add_argument('--download', type=str2bool, nargs='?',
                            const=True, default=False,
                            help="Activate download mode.")
        parser.add_argument('--scores', type=str2bool, nargs='?',
                            const=True, default=True,
                            help="Show scores in output matrix.")
        return parser.parse_args()


    # parse and config arguments
    args = arg_parser()

    plot_matrix(args.uniprot, plotscores=args.scores, zoom=args.zoom, download=args.download, folder=args.folder, pdbfilter=args.pdb)
